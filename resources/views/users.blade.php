@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12" style="margin-bottom: 15px;">
                                    <table id="grid_section"></table>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset("assets/js/jquery-3.1.1.js")}}"></script>
    <script src="{{asset("assets/js/bootstrap.min.js")}}"></script>
{{--    <script src="{{asset("assets/js/datatable.bootstrap.min.js")}}"></script>--}}
{{--    <script src="{{asset("assets/js/jquery.datatable.min.js")}}"></script>--}}
    <script src="{{asset("assets/js/jquery-ui.js")}}"></script>
    <script src="{{asset("assets/jqgrid/jqgrid.min.js")}}"></script>
    <script>

        $grid = $("#grid_section");
        $grid.jqGrid({
            datatype: 'json',
            'url':'{{route('home')}}',
            colModel: [
                {name: "name" , label: "name", align: 'center', search: true},
                {name: "email" ,  label: 'Email', align: 'center', search: true},
                {name: "details" ,  label: 'Details', align: 'center', search: true},
                {name: "avatar", label: "Avatar",align: "center", search: true},{"data": "amount"},
                {name: "Disable" ,  label: 'Disable',    align: 'center', search:false,sortable: false, formatter:disable},
                {name: "Remove" ,  label: 'Remove',    align: 'center', search:false,sortable: false, formatter:remove},
            ],

            pager: true,
            viewrecords: true,
            rownumbers: true,
            altRows: true,
            sortorder : "asc",
            scrollerbar:true,
            height:'400',
            forceFit:true,
            autowidth:true,
            loadonce: true,
            shrinkToFit: false,
            title: false,
            rowNum: '30',
            rowList: [5, 10, 30, 50, 100, "10000:All" ],
            caption: "Users",

        });
        $grid.jqGrid('filterToolbar', { searchOnEnter: false, enableClear: true ,height:40,odata:true});

        function disable(cellVal,options,rowObject) {
            return "<input class='btn btn-info btn-xs change-status'  type='button' value='Disable'   />";
        };
        function remove(cellVal,options,rowObject) {
            return "<input class='btn btn-danger btn-xs'  type='button' value='Remove'   />";
        };
    </script>
@endsection
