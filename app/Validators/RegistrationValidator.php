<?php



namespace App\Validators;


use Illuminate\Foundation\Http\FormRequest;

class RegistrationValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:customers',
            'details' => ['required', 'string'],
            'avatar' => ['required', 'string'],
            'password' => ['required', 'string', 'min:4', 'confirmed'],
            'g-recaptcha-response' => ['required'], //TODO: check recaptcha not only required
        ];
    }

    public function messages()
    {
        $message = [
            'g-recaptcha-response.required' => __('Captcha Requaired'),
        ];

        return $message;
    }

}
