<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends  Authenticatable
{
    use  SoftDeletes;

    protected $fillable=['name','email','details','avatar', 'password',];
    protected $hidden = [
        'password',
    ];
}
